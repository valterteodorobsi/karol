import { Component } from '@angular/core';

@Component({
  selector: 'app-text-interpolation',
  standalone: true,
  imports: [],
  templateUrl: './text-interpolation.component.html',
  styleUrl: './text-interpolation.component.scss'
})
export class TextInterpolationComponent {
  public name = "Leandro Shoiti";

  public addFiveMore(value: number) {
    return value + 5;
  }
}
