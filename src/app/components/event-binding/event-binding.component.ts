import { Component } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  standalone: true,
  imports: [],
  templateUrl: './event-binding.component.html',
  styleUrl: './event-binding.component.scss'
})
export class EventBindingComponent {
  public value = 0;
  public centerX = 0;
  public centerY = 0;

  public add() {
    this.value++;
  }

  public subtract() {
    this.value--;
  }

  public onMouseMove(event: MouseEvent) {
    this.centerX = event.clientX;
    this.centerY = event.clientY;
  }
}
