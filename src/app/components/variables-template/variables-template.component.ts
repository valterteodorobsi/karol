import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { ChildComponent } from './child/child.component';

@Component({
  selector: 'app-variables-template',
  standalone: true,
  imports: [ ChildComponent ],
  templateUrl: './variables-template.component.html',
  styleUrl: './variables-template.component.scss'
})
export class VariablesTemplateComponent implements AfterViewInit {
  // Decorator @ViewChild pega os dados do HTML
  @ViewChild('name') public nameLabel!: ElementRef;
  @ViewChild('teste') public testeInput!: ElementRef;
  @ViewChild(ChildComponent) public childComponent!: ChildComponent;

  ngAfterViewInit(): void {
    /*
     * Ciclo de vida
     * Quando a View carrega, essa função é executada!
     */
    console.log('nameLabel:', this.nameLabel.nativeElement.innerText);
    console.log('testeInput:', this.testeInput.nativeElement.value);
    console.log('childComponent:', this.childComponent.name);
  }

}
