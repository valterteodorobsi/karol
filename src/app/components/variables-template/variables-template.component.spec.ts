import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VariablesTemplateComponent } from './variables-template.component';

describe('VariablesTemplateComponent', () => {
  let component: VariablesTemplateComponent;
  let fixture: ComponentFixture<VariablesTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [VariablesTemplateComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(VariablesTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
