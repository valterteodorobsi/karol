import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, DoCheck, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-events',
  standalone: true,
  imports: [],
  templateUrl: './events.component.html',
  styleUrl: './events.component.scss'
})
export class EventsComponent implements OnChanges, OnInit, AfterContentInit, AfterViewInit, DoCheck, AfterContentChecked, AfterViewChecked {

  // É o input que recebemos com parametro da tag desse componente. Ex.: <app-events title=""></app-events>
  @Input() public title?: string;

  public value: number = 0;

  ngOnChanges(changes: SimpleChanges): void {
    console.log("ngOnChanges inicia toda vez que um @Input é introduzido ou alterado");
  }

  ngOnInit(): void {
    console.log("ngOnInit inicializa uma vez junto com o componente");
  }

  ngAfterContentInit(): void {
    console.log("ngAfterContentInit inicializa uma vez quando o componente está pronto para renderizar");
  }

  ngAfterViewInit(): void {
    console.log("ngAfterViewInit inicializa uma vez quando o componente terminar de renderizar a view para o usuário");
  }

  ngDoCheck(): void {
    console.log("ngDoCheck inicia toda vez que ocorre uma detecção de mudanças");
  }

  ngAfterContentChecked(): void {
    console.log("ngAfterContentChecked inicia toda vez que ocorre uma detecção de mudanças, mas somente depois que o componente estiver pronto para renderizar");
  }

  ngAfterViewChecked(): void {
    console.log("ngAfterViewChecked inicia toda vez que ocorre uma detecção de mudanças, mas somente depois que o componente renderizar a view para o usuário");
  }

  ////////////////////////////////////////////////////

  public add(): void {
    this.value++;
  }
}
