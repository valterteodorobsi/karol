import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-ngclass-ngstyle-template',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './ngclass-ngstyle-template.component.html',
  styleUrl: './ngclass-ngstyle-template.component.scss'
})
export class NgclassNgstyleTemplateComponent {
  public size = 18;

  public add() {
    this.size++;
  }

  public subtract() {
    this.size--;
  }

}
