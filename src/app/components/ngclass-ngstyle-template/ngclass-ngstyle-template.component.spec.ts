import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgclassNgstyleTemplateComponent } from './ngclass-ngstyle-template.component';

describe('NgclassNgstyleTemplateComponent', () => {
  let component: NgclassNgstyleTemplateComponent;
  let fixture: ComponentFixture<NgclassNgstyleTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NgclassNgstyleTemplateComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NgclassNgstyleTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
