import { Component } from '@angular/core';

/*
 * Segue a ordem de prioridade dos styles da maior prioridade para o menor
 * 1. styles do componente
 * 2. styleUrl do componente
 * 3. src/styles.scss - Style Global
 * 
 * Obs.: 
 *  Caso existe um componente sendo renderizado dentro, o style do componente não interfere no outro.
 */

@Component({
  selector: 'app-style',
  standalone: true,
  imports: [],
  templateUrl: './style.component.html',
  styleUrl: './style.component.scss',
  styles: `
    // Prioridade 1
    p {
      color: green;
    }
  `
})
export class StyleComponent {

}
