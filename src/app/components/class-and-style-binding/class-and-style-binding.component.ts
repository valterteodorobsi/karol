import { Component } from '@angular/core';

@Component({
  selector: 'app-class-and-style-binding',
  standalone: true,
  imports: [],
  templateUrl: './class-and-style-binding.component.html',
  styleUrl: './class-and-style-binding.component.scss'
})
export class ClassAndStyleBindingComponent {
  public textColor = 'white';
}
