import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-template',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './template.component.html',
  styleUrl: './template.component.scss'
})
export class TemplateComponent {
  public ifValue: number = 1;
  public switchValue: string = 'B';

  public itens: Array<{ name: string }> = [
    { name: "Leandro" },
    { name: "Leonardo" },
    { name: "Leo" },
    { name: "Ivo" }
  ]

  public arrayEmpty: Array<{ name: string }> = []

  public trackByFn(index: number) {
    return index;
  }
}
