import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleIsolationComponent } from './style-isolation.component';

describe('StyleIsolationComponent', () => {
  let component: StyleIsolationComponent;
  let fixture: ComponentFixture<StyleIsolationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [StyleIsolationComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(StyleIsolationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
