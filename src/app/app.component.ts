import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

// Componentes
import { StyleComponent } from './components/style/style.component';
import { StyleIsolationComponent } from './components/style-isolation/style-isolation.component';
import { TextInterpolationComponent } from './components/text-interpolation/text-interpolation.component';
import { PropertyBindingComponent } from './components/property-binding/property-binding.component';
import { AttributeBindingComponent } from './components/attribute-binding/attribute-binding.component';
import { ClassAndStyleBindingComponent } from './components/class-and-style-binding/class-and-style-binding.component';
import { EventBindingComponent } from './components/event-binding/event-binding.component';
import { TwoWayBindingComponent } from './components/two-way-binding/two-way-binding.component';
import { NgclassNgstyleTemplateComponent } from './components/ngclass-ngstyle-template/ngclass-ngstyle-template.component';
import { VariablesTemplateComponent } from './components/variables-template/variables-template.component';
import { TemplateComponent } from './components/template/template.component';
import { EventsComponent } from './components/events/events.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    StyleComponent,
    StyleIsolationComponent,
    TextInterpolationComponent,
    PropertyBindingComponent,
    AttributeBindingComponent,
    ClassAndStyleBindingComponent,
    EventBindingComponent,
    TwoWayBindingComponent,
    NgclassNgstyleTemplateComponent,
    VariablesTemplateComponent,
    TemplateComponent,
    EventsComponent
  ],
  template: `
    <router-outlet></router-outlet>
    <h1>Estudos de Angular</h1>
    <app-style />

    <div class="theme-dark">
      <app-style-isolation />
    </div>

    <app-text-interpolation />

    <app-property-binding />

    <app-attribute-binding />

    <app-class-and-style-binding />

    <app-event-binding />

    <app-two-way-binding />

    <app-ngclass-ngstyle-template />

    <app-variables-template />

    <app-template />

    <app-events title="Teste prop"><h1>TESTESTESTES</h1></app-events>
  `,
})
export class AppComponent {}
